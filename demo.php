#!/usr/bin/php
<?php

/*
  The first line in this file does the 'trick'
  for using this code beside the location it is executed,
  first You need to add permissions for exetue this file.
  Do the folowing in Your bash:
  1. Go to location where this file is stored in.
  2. Type command: chmod +x demo.php
  3. Now You can run this code wherever You are in, just type: ./demo.php <command>
  -- please note that this works well on Unix and Linux systems, nor Windows
 */

require 'vendor/autoload.php';

$param = $argv[1];

$user = new \Demo\User();
$db = new \Demo\DbJson();

switch ($param) {
  case 'list':
    echo $user->print($db->getAllFromDb());
    break;

  case 'find':
    $allUsers = $db->getAllFromDb();
    $userId = $user->findByName($argv[2], $allUsers);
    echo $user->print($user->list($userId, $allUsers));
    break;

  case 'languages':
    // remove first argument (the filename) from the array
    // and second argument (the option name) from the array
    unset($argv[0], $argv[1]);
    // now we have the values of searched programming skills languages
    $dbArray = $db->getDbArray();
    $mappings = $dbArray['data']['userProgrammingLanguages'];
    $language = new \Demo\Language($db);
    // find ids' for searched languages
    $languageIds = $language->find($argv);
    $userId = $user->findByLanguage($languageIds, $mappings);
    echo $user->print($user->list($userId, $db->getAllFromDb()));
    break;

  case 'addPerson':
    $firstName = $argv[2];
    $lastName = $argv[3];
    // remove first fourth elements from argv array, now we have only languages
    unset($argv[0], $argv[1], $argv[2], $argv[3]);
    $languages = $argv;
    $user->add($firstName, $lastName, $languages);
    break;

  case 'removePerson':
    $userId = (int) $argv[2];
    $user->remove($userId);
    break;

  case 'addLanguage':
    $languageName = $argv[2];
    $language = new \Demo\Language($db);
    $language->add($languageName);
    break;

  case 'removeLanguage':
    $languageName = $argv[2];
    $language = new \Demo\Language($db);
    $language->remove($languageName);
    break;

  default:
    echo "Specified parameter '$param' is not a app command!\n";
    break;
}
