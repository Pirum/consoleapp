# README #

Simple console Application, ready to use in Unix/Linux systems.

### Used technology ###

* Objective PHP 7.1
* PHP CLI
* Database as file stored in local hard drive, file format: JSON

### Description ###

Application is build for handling users (name and surname) and their programming language skills.
The example output is like:
ID. Name Surname - (language1, language2, ...)

### List of commands ###

* ./demo.php list
* ./demo.php find *username
* ./demo.php languages language1 language2 language3 ...
* ./demo.php addPerson Name Surname language1 language2 language3 ...
* ./demo.php removePerson ID
* ./demo.php addLanguage name
* ./demo.php removeLanguage name

### Things to do in the future (improvements) ###

* the programming languages shouldn't be case-sensitive
* finding method should be moved into DbJson class
* method for db access should implements interface for smooth technology switch
* db operations (like save, update, remove) should works on objects, i.e. $service->persist($person); $service->update($language)
* fiels validation (name and surname should contain only letters and not blank, language name unique and not empty)
* exception handling errors while database operation fails
* on save new person, when new language name will be typed, it should automatically be created in db