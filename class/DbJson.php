<?php

namespace Demo;

/**
 * DbJson class.
 */
class DbJson
{
    /**
     * @var array
     */
    protected $dbArray;

    /**
     * @var array
     */
    protected $schema;

    /**
     * @var object
     */
    protected $config;

    /**
     * @return array
     */
    public function getDbArray()
    {
        return $this->dbArray;
    }

    /**
     * @return array
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * constructor.
     */
    public function __construct()
    {
        $config = new Config();
        $this->config = $config;
        // read json file
        $json = file_get_contents($config->databasePath);
        $this->dbArray = json_decode($json, true);
        $this->schema = $this->dbArray['schema'];
    }

    /**
     * Get all users and connect them with their programming skills.
     *
     * @return array
     */
    public function getAllFromDb(): array
    {
        $data = $this->dbArray['data'];
        // users from db
        $users = $data['users'];
        // languages from db
        $languages = $data['programmingLanguages'];
        // mappings user and programming language
        $mappings = $data['userProgrammingLanguages'];

        // connect user with programming language based on mappings
        foreach ($users as $key => $user) {
            $userLanguages = array();
            foreach ($mappings as $mapping) {
                if ($mapping['userId'] === $user['id']) {
                    foreach ($languages as $language) {
                        if ($language['id'] === $mapping['programmingLanguagesId']) {
                            $userLanguages[] = $language['language'];
                        }
                    }
                }
            }

            // add into users array his programming lanugages skills
            $users[$key]['languages'] = $userLanguages;
        }

        return $users;
    }

    /**
     * Add new language.
     *
     * @param string $languageName [description]
     *
     * @return bool [description]
     */
    public function addLanguage(string $languageName = ''): bool
    {
        if ('' == $languageName) {
            return false;
        }

        $tempArray = $this->getDbArray();
        // get next id key value
        $nextId = $this->getAutoIncrementNextKey('programmingLanguages', 'id');
        // prepare data to push into array and push them
        $dataToPush = array('id' => $nextId, 'language' => $languageName);
        array_push($tempArray['data']['programmingLanguages'], $dataToPush);

        return $this->storeIntoDb($tempArray, 'programmingLanguages', 'id');
    }

    /**
     * Add new user.
     *
     * @param string $name      [description]
     * @param string $surname   [description]
     * @param array  $languages [description]
     *
     * @return bool [description]
     */
    public function addUser(string $name, string $surname, array $languages): bool
    {
        if ('' == $name || '' == $surname || !is_array($languages)) {
            return false;
        }

        $tempArray = $this->getDbArray();
        // get next id key value
        $nextUserId = $this->getAutoIncrementNextKey('users', 'id');
        $nextMappingId = $this->getAutoIncrementNextKey('userProgrammingLanguages', 'id');
        // prepare user data to push into array and push them
        $userToPush = array(
          'id' => $nextUserId,
          'name' => $name,
          'surname' => $surname,
        );
        array_push($tempArray['data']['users'], $userToPush);
        // save data into db and update autoincrement key
        $result = $this->storeIntoDb($tempArray, 'users', 'id');

        // prepare mappings data to push into array and push them
        foreach ($languages as $language) {
            // get language id
            $languageId = 0;
            foreach ($tempArray['data']['programmingLanguages'] as $dbLanguage) {
                if ($dbLanguage['language'] == $language) {
                    $languageId = $dbLanguage['id'];
                }
            }
            $mappingToPush = array(
              'id' => $nextMappingId,
              'userId' => $nextUserId,
              'programmingLanguagesId' => $languageId,
            );
            array_push($tempArray['data']['userProgrammingLanguages'], $mappingToPush);
        }

        // save data into db and update autoincrement key
        $result = $this->storeIntoDb($tempArray, 'userProgrammingLanguages', 'id');

        return $result;
    }

    /**
     * remove language by its name.
     *
     * @param string $name [description]
     *
     * @return bool [description]
     */
    public function removeLanguage(string $name = ''): bool
    {
        if ('' == $name) {
            return false;
        }

        $tempArray = $this->getDbArray();
        $isRemoved = false;
        foreach ($tempArray['data']['programmingLanguages'] as $key => $table) {
            if ($table['language'] == $name) {
                unset($tempArray['data']['programmingLanguages'][$key]);
                $isRemoved = true;
            }
        }
        if ($isRemoved) {
            return $this->storeIntoDb($tempArray);
        }

        return false;
    }

    /**
     * remove user by its id value.
     *
     * @param int $id [description]
     *
     * @return bool [description]
     */
    public function removeUser(int $id): bool
    {
        if ('' == $id) {
            return false;
        }

        $id = (int) $id;

        $tempArray = $this->getDbArray();
        $isRemoved = false;
        // remove user
        foreach ($tempArray['data']['users'] as $key => $table) {
            if ($table['id'] === $id) {
                unset($tempArray['data']['users'][$key]);
                $isRemoved = true;
            }
        }
        // remove all foreign keys with languages (but not languages)
        foreach ($tempArray['data']['userProgrammingLanguages'] as $key => $table) {
            if ($table['userId'] === $id) {
                unset($tempArray['data']['userProgrammingLanguages'][$key]);
            }
        }

        // update database
        if ($isRemoved) {
            return $this->storeIntoDb($tempArray);
        }

        return false;
    }

    /**
     * Get auto increment next key value.
     *
     * @param string $table  [description]
     * @param string $column [description]
     *
     * @return mixed [description]
     */
    protected function getAutoIncrementNextKey(string $table, string $column)
    {
        $dbTables = $this->schema['tables'];

        foreach ($dbTables as $dbTable) {
            if ($dbTable['tableName'] == $table) {
                foreach ($dbTable['columns'] as $dbColumn) {
                    if ($dbColumn['columnName'] == $column) {
                        return $dbColumn['autoincrement']['nextKey'];
                    }
                }
            }
        }

        return false;
    }

    /**
     * Update database.
     *
     * @param array  $data   [description]
     * @param string $table  [description]
     * @param string $column [description]
     *
     * @return bool [description]
     */
    protected function storeIntoDb(array $data, string $table = '', string $column = ''): bool
    {
        if ('' != $table && '' != $column) {
            $data = $this->updateAutoIncrementKey($data, $table, $column);
        }

        $jsonData = json_encode($data);
        if (file_put_contents($this->config->databasePath, $jsonData)) {
            return true;
        }

        return false;
    }

    /**
     * Update auto increment key value.
     *
     * @param array  $data   [description]
     * @param string $table  [description]
     * @param string $column [description]
     *
     * @return mixed [description]
     */
    protected function updateAutoIncrementKey(array $data, string $table, string $column)
    {
        $dbTables = $this->schema['tables'];

        foreach ($dbTables as $tableKey => $dbTable) {
            if ($dbTable['tableName'] == $table) {
                foreach ($dbTable['columns'] as $columnKey => $dbColumn) {
                    if ($dbColumn['columnName'] == $column) {
                        $nextKey = $dbColumn['autoincrement']['nextKey'] + 1;

                        $data['schema']['tables'][$tableKey]['columns'][$columnKey]['autoincrement']['nextKey'] = $nextKey;

                        return $data;
                    }
                }
            }
        }

        return false;
    }
}
