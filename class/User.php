<?php

namespace Demo;

/**
 * User class.
 */
class User
{
    /**
     * @var array
     */
    private $allUsers;

    /**
     * @var object
     */
    protected $db;

    /**
     * constructor.
     */
    public function __construct()
    {
        $db = new \Demo\DbJson();
        $this->db = $db;
        $this->allUsers = $db->getDbArray();
    }

    /**
     * [list description].
     *
     * @param array $arrayIds
     * @param array $allUsers
     *
     * @return array
     */
    public function list(array $arrayIds, array $allUsers): array
    {
        $result = array();
        foreach ($allUsers as $key => $user) {
            foreach ($arrayIds as $id) {
                if ($user['id'] === $id) {
                    $result[] = $allUsers[$key];
                }
            }
        }

        return $result;
    }

    /**
     * Find users by its name.
     *
     * @param string $needle
     * @param array  $stack
     *
     * @return array
     */
    public function findByName(string $needle, array $stack): array
    {
        $foundedUserIds = array();
        foreach ($stack as $user) {
            $fullName = $user['name'].' '.$user['surname'];
            if (false !== strpos($fullName, $needle)) {
                $foundedUserIds[] = $user['id'];
            }
        }

        return $foundedUserIds;
    }

    /**
     * Find users by programming languages.
     *
     * @param array $needle
     * @param array $stack
     *
     * @return array
     */
    public function findByLanguage(array $needle, array $stack): array
    {
        // how many languages typed into search
        $numberOfLanguages = count($needle);
        // count number of users repeated
        $counts = array_count_values(array_column($stack, 'userId'));

        // collect userIds where count of their programming languages is
        // equal or greater than number of searching languages.
        $usersPassed = array();
        foreach ($counts as $userId => $count) {
            if ($count >= $numberOfLanguages) {
                $usersPassed[] = $userId;
            }
        }

        // if searches contains more languages than users have defined,
        // there is nothing to search.
        if (count($usersPassed) > 0) {
            // for $usersPassed check if they have programming languages what we are looking for
            for ($i = 0; $i < $numberOfLanguages; ++$i) {
                // clear results array while we are searching next programming language
                $results = array();
                foreach ($usersPassed as $key => $userId) {
                    foreach ($stack as $row) {
                        if ($row['userId'] == $userId && $row['programmingLanguagesId'] == $needle[$i]) {
                            // collect user id only if it passed the condition
                            $results[] = $userId;
                        }
                    }
                }
                // update array $usersPassed by filtered results
                $usersPassed = $results;
            }

            return $usersPassed;
        }
        // empty
        return array();
    }

    /**
     * Print users list with programming languages.
     *
     * @param array $array
     *
     * @return string
     */
    public function print(array $array): string
    {
        $print = "\nID. Name Surname - (programming languages)\n\n";
        foreach ($array as $row) {
            $print .= $row['id'].'. '.$row['name'].' '.$row['surname'].' - (';
            foreach ($row['languages'] as $language) {
                $print .= $language;
                if ($language !== end($row['languages'])) {
                    $print .= ', ';
                }
            }
            $print .= ")\n";
        }

        return $print;
    }

    /**
     * Add new user into db.
     *
     * @param string $name      [description]
     * @param string $surname   [description]
     * @param array  $languages [description]
     *
     * @return bool [description]
     */
    public function add(string $name, string $surname, array $languages): bool
    {
        return $this->db->addUser($name, $surname, $languages);
    }

    /**
     * Remove user by id.
     *
     * @param int $id [description]
     *
     * @return bool [description]
     */
    public function remove(int $id): bool
    {
        return $this->db->removeUser($id);
    }
}
