<?php

namespace Demo;

/**
 * Language class.
 */
class Language
{
    /**
     * @var object
     */
    protected $db;

    /**
     * @param DemoDbJson $db [description]
     */
    public function __construct(\Demo\DbJson $db)
    {
        $this->db = $db;
    }

    /**
     * [find description].
     *
     * @param array $array
     *
     * @return array
     */
    public function find(array $array): array
    {
        $dbArray = $this->db->getDbArray();
        $dbData = $dbArray['data'];
        $languages = $dbData['programmingLanguages'];

        $results = array();
        foreach ($array as $needle) {
            foreach ($languages as $language) {
                if (strtolower($needle) === $language['language']) {
                    $results[] = $language['id'];
                }
            }
        }

        return $results;
    }

    /**
     * add new language.
     *
     * @param string $name language name
     *
     * @return bool
     */
    public function add(string $name): bool
    {
        return $this->db->addLanguage($name);
    }

    /**
     * remove existing language.
     *
     * @param string $name language name to remove
     *
     * @return bool
     */
    public function remove(string $name): bool
    {
        return $this->db->removeLanguage($name);
    }
}
